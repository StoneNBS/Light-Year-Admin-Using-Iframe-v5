# Light Year Admin Using Iframe v5

#### 介绍
该项目是光年后台管理模板(Light Year Admin Using Iframe v5)基于Bootstrap 5.1.3版本。

Light Year Admin Using Iframe v5是基于Bootstrap 5.1.3的后台管理系统模板，当然你需要知道的是你无法从v4升级到v5，至于为什么，这完全不能怪我，Bootstrap 5实际上跟Bootstrap 4的差别很大，首先对栅格的相应拐点做了更多的细分，其次它不再依赖于jquery，再次呢，它对css中的class命名做了不少的更改，最后，5的版本在css中用到了大量的变量，这是4里面不成用到的。

之前的v5版本因为是以ajax加载，在之后的测试中发现问题比较多，这里还是以iframe版本为主。此前的ajax版本v5暂时放弃。

#### 不能直接升级
#### 不能直接升级
#### 不能直接升级

目前v5没有前面的项目一样有做示例演示和其他很多插件的整合，后期看自己的时间。

#### 演示网址
[http://lyear.itshubao.com/iframe/v5](http://lyear.itshubao.com/iframe/v5)

#### 交流群
![输入图片说明](https://images.gitee.com/uploads/images/2021/0419/100528_9d22ac7d_82992.png "光年后台模板交流群群聊二维码.png")

#### 最新进展
2022.11.01 v5的iframe版本项目基本完工。


### 意见提交

目前项目正处于完善阶段，在此过程中如果有任何的意见和建议请提供提交issue联系我们活或者进群@我。
